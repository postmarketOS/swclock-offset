install:
	install -Dm 755 src/swclock-offset-boot.sh \
		$(DESTDIR)/usr/bin/swclock-offset-boot
	install -Dm 755 src/swclock-offset-shutdown.sh \
		$(DESTDIR)/usr/bin/swclock-offset-shutdown

	# openrc:
	install -Dm 755 openrc/swclock-offset-boot.initd \
		$(DESTDIR)/etc/init.d/swclock-offset-boot
	install -Dm 755 openrc/swclock-offset-shutdown.initd \
		$(DESTDIR)/etc/init.d/swclock-offset-shutdown

	# systemd
	install -Dm 644 systemd/swclock-offset-boot.service \
		$(DESTDIR)/usr/lib/systemd/system/swclock-offset-boot.service
	install -Dm 644 systemd/swclock-offset-shutdown.service \
		$(DESTDIR)/usr/lib/systemd/system/swclock-offset-shutdown.service
